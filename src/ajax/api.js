import service from "../utils/request";

//检索问题
export function searchProblem(searchKey) {
    return service.get('/markDown/searchProblem', {
        headers: { 'Content-Type': 'application/json' },
        params: {
            searchKey: searchKey
        }
    })
}

export function findMarkDownBypath(path) {
    return service.get('/markDown/findMarkDownBypath', {
        headers: { 'Content-Type': 'application/json' },
        params: {
            path: path
        }
    })
}

//检索问题
export function searchProblemV2(searchKey) {
    return service.get('/markDownV2/searchProblemV2', {
        headers: { 'Content-Type': 'application/json' },
        params: {
            searchKey: searchKey
        }
    })
}

export function findMarkDownBypathV2(path) {
    return service.get('/markDownV2/findMarkDownBypath', {
        headers: { 'Content-Type': 'application/json' },
        params: {
            path: path
        }
    })
}

//检索问题
export function searchProblemV3(searchKey, searchSetting) {
    return service.get('/markDownV3/searchProblem', {
        headers: { 'Content-Type': 'application/json' },
        params: {
            searchKey: searchKey,
            searchSetting: searchSetting
        }
    })
}

export function findMarkDownBypathV3 (obj) {
    return service.post('/markDownV3/findMarkDownBypath', JSON.stringify(obj), {
        headers: { 'Content-Type': 'application/json' }
    })
}

export function addKeyWord (obj) {
    return service.post('/markDownV3/addKeyWord', JSON.stringify(obj), {
        headers: { 'Content-Type': 'application/json' }
    })
}

export function deleteKeyWord (obj) {
    return service.post('/markDownV3/deleteKeyWord', JSON.stringify(obj), {
        headers: { 'Content-Type': 'application/json' }
    })
}

export function findMarkDownByKey(handle) {
    return service.get('/markDownV3/findMarkDownByKey', {
        headers: { 'Content-Type': 'application/json' },
        params: {
            handle: handle
        }
    })
}
