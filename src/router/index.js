
import Vue from "vue"
import Router from 'vue-router'
import Entry from "../components/Entry.vue"
import AudioToWord from "@/view/AudioToWord"
import WordToAudio from "@/view/WordToAudio"
import WordToAudioV2 from "@/view/WordToAudioV2"
import MarkDown from "@/view/MarkDown"
import MarkDownV2 from "@/view/MarkDownV2"
import MarkDownV3 from "@/view/MarkDownV3"

Vue.use(Router)
export default new Router({
    routes: [
        {
            path: '/',
            redirect: 'entry'
        },
        {
            path: "/entry",
            name: "Entry",
            component: Entry
        },
        {
            path: "/audioToWord",
            name: "AudioToWord",
            component: AudioToWord
        },
        {
            path: "/wordToAudio",
            name: "WordToAudio",
            component: WordToAudio
        },
        {
            path: "/wordToAudioV2",
            name: "WordToAudioV2",
            component: WordToAudioV2
        },
        {
            path: "/markDown",
            name: "markDown",
            component: MarkDown
        },
        {
            path: "/markDownV2",
            name: "markDownV2",
            component: MarkDownV2
        },
        {
            path: "/markDownV3",
            name: "markDownV3",
            component: MarkDownV3
        },
    ]
})
