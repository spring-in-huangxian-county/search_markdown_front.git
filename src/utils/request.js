import axios from 'axios'

// 创建axios实例
const service = axios.create({
  // baseURL: 'http://192.168.0.198:9000', // api的base_Url
//   baseURL: defaultValue.baseURL, // 本机
  baseURL: 'http://10.160.1.77:9000/back', // 本机
  // baseURL: 'http://47.92.53.108:9000', // 部署到服务器放开本代码，请求路径为服务器公网ip+后端启动端口
  timeout: 50000 // 请求超时时间
})

// 请求拦截器
axios.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 响应拦截器
axios.interceptors.response.use(
  function (config) {
    // 对响应数据做点什么
    return config
  },
  function (error) {
    // 对响应错误做点什么
    return Promise.reject(error)
  }
)
// 暴露方法，暴露之后vue才能检测到
export default service
